#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
int main(int argc, char *argv[])
{
    const int tag = 42;
    int id, ntasks, source_id, dest_id, err, i;
    MPI_Status status;
    int msg[2];

    err = MPI_Init(&argc, &argv);
    if (err != MPI_SUCCESS)
    {
        printf("MPI initialization failed!\n");
        exit(1);
    }
    err = MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
    err = MPI_Comm_rank(MPI_COMM_WORLD, &id);
    if (ntasks < 2)
    {
        printf("You have to use at least 2 processors to run this program\n");
        MPI_Finalize();
        exit(0);
    }
    if (id == 0)
    {
        for (i = 1; i < ntasks; i++)
        {
            err = MPI_Recv(msg, 2, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD,
                           &status);
            source_id = status.MPI_SOURCE;
            printf("Received message %d %d from process %d\n", msg[0], msg[1],
                   source_id);
        }
    }
    else
    {
        msg[0] = id;
        msg[1] = ntasks;
        dest_id = 0;
        err = MPI_Send(msg, 2, MPI_INT, dest_id, tag, MPI_COMM_WORLD);
    }
    if (id == 0)
        printf("Ready\n");
    err = MPI_Finalize();

    exit(0);
    return 0;
}