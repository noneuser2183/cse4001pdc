#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int i, maxA, minA;
    int a[] = {1, 4, 2, 5, 3, 9, 8, 6, 7};

#pragma omp parallel for reduction(max \
                                   : maxA)
    for (i = 0; i < 9; ++i)
    {
        maxA = maxA > a[i] ? maxA : a[i];
    }

#pragma omp parallel for reduction(min \
                                   : minA)
    for (i = 0; i < 9; ++i)
    {
        minA = minA < a[i] ? minA : a[i];
    }

    printf("Maximum Element in array: %d\nMinimum Element in array: %d", maxA, minA);
    return 0;
}