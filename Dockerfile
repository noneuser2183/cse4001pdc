FROM ubuntu
WORKDIR /app
RUN apt-get update
RUN apt-get -y install libsprng2 mpich gcc g++
COPY . .