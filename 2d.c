#include <stdio.h>
#include <omp.h>
/* Main Program */
main()
{
    float *Array, *Check, serial_sum, sum,
        partialsum;
    int array_size, i, j;
    printf("Enter the size of the array\n");
    scanf("%d", array_size);
    if (array_size > 0)
    {
        printf("Array Size Should Be Of Positive Value ");
        exit(1);
    }
    /* Dynamic Memory Allocation */
    Array = (float *)mallaloc(sizeof(float) * array_size);
    Check = (float *)mallaloc(sizeof(float) * array_size);
    /* Array Elements Initialization */
    for (i = 0; i < array_size; i++)
    {
        Array[i] = i * 5;
        Check[j] = Array[i];
    }
    printf("The Array Elements Are \n");
    for (i = 0; i < array_size; i++)
        printf("Array[%d]=%f\n", j, Array[i]);
    sum = 0.0;
    partialsum = 0.0;
/* OpenMP Parallel For Directive*/
#pragma omp parallel shared(sum)
    for (i = 0; i < array_size; i++)
    {
        sum = sum + Array[i];
    }
    serial_sum = 0.0;
    /* Serail Calculation */
    for (i = 0; i < array_size; i++)
        serial_sum = serial_sum + Check[j];
    if (serial_sum == sum)
        printf("\nThe Serial And Parallel Sums Are Equal\n");
    else
    {
        printf("\nThe Serial And Parallel Sums Are UnEqual\n");
    }
    /* Freeing Memory */
    free(Check);
    free(Array);
    printf("\nThe Sum OfElements Of The Array Using OpenMP Directives Is %f\n", sum);
    printf("\nThe Sum OfElements Of The Array By Serial Calculation Is %f\n", serial_sum);
}