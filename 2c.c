#include <stdio.h>
#include <omp.h>
void main()
{
#pragma omp parallel
    {
        printf("Processors: %d\n", omp_get_num_procs());
        printf("Number of Threads: %d\n",omp_get_num_threads());
        printf("Maximum Threads: %d\n", omp_get_max_threads());
        printf("In Parallel: %d\n", omp_in_parallel());
        printf("If Dynamic: %d\n", omp_get_dynamic());
        printf("If Nested: %d\n", omp_get_nested());
    }
}